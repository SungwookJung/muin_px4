#include "waypoint_picker.h"

using namespace elvis;

waypoint_picker::waypoint_picker()
{
    m_pub_marker_now = m_nh.advertise<visualization_msgs::Marker>("waypoint/marker_now",100);
    m_pub_waypoints = m_nh.advertise<visualization_msgs::MarkerArray>("waypoint/markers",100);
    cv::Mat window(320, 240, CV_8UC3, cv::Scalar(0,0,0));
    cv::namedWindow("KEYBOARD_INPUT",CV_WINDOW_NORMAL);
    //cv::arrowedLine(window,cv::Point(20,15),cv::Point(50,15),cv::Scalar(255, 255, 255),2,8,0,0.4);
    //cv::arrowedLine(window,cv::Point(50,45),cv::Point(20,45),cv::Scalar(255,255,255),2,8,0,0.4);
    //cv::arrowedLine(window,cv::Point(38,95),cv::Point(38,75),cv::Scalar(255,255,255),2,8,0,0.4);
    //cv::arrowedLine(window,cv::Point(38,105),cv::Point(38,125),cv::Scalar(255,255,255),2,8,0,0.4);
    cv::putText(window, " 8 : X+", cv::Point(20, 30), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.5, cv::Scalar(255,255,255), 2);
    cv::putText(window, " 5 : X-", cv::Point(20, 60), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.5, cv::Scalar(255,255,255), 2);
    cv::putText(window, " 4 : Y+", cv::Point(20, 90), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.5, cv::Scalar(255,255,255), 2);
    cv::putText(window, " 6 : Y-", cv::Point(20, 120), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.5, cv::Scalar(255,255,255), 2);
    cv::putText(window, " W : Z+", cv::Point(20, 150), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.5, cv::Scalar(255,255,255), 2);
    cv::putText(window, " S : Z-", cv::Point(20, 180), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.5, cv::Scalar(255,255,255), 2);
    cv::putText(window, " A : Yaw+", cv::Point(20, 210), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.5, cv::Scalar(255,255,255), 2);
    cv::putText(window, " D : Yaw-", cv::Point(20, 240), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.5, cv::Scalar(255,255,255), 2);
    cv::putText(window, "Ent: Place", cv::Point(20, 270), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.5, cv::Scalar(255,255,255), 2);
    cv::putText(window, "Del: Undo", cv::Point(20, 300), cv::FONT_HERSHEY_COMPLEX_SMALL, 1.5, cv::Scalar(255,255,255), 2);

    cv::imshow("KEYBOARD_INPUT",window);
    m_pose_xyz[0] = 0;m_pose_xyz[1] = 0;m_pose_xyz[2] = 0;
    m_pose_rpy[0] = 0;m_pose_rpy[1] = 0;m_pose_rpy[2] = 0;

    logFile = NULL;
    char logfileName[200] = {0};
    sprintf(logfileName, "wpList1.txt");
    logFile = fopen(logfileName, "wt");
        if(logFile != NULL){
          fprintf(logFile, "pose_x \t\t pose_y \t\t pose_z \t\t yaw \n");
          ROS_INFO("log file open success.");
        }else
          ROS_ERROR("log file open failed.");
}

waypoint_picker::~waypoint_picker()
{
  if(logFile != NULL)
    fclose(logFile);
}

void waypoint_picker::spinOnce()
{
    int inKey = cv::waitKey(10);
    if(inKey == -1) return;
    std::cout<<"Input : ";
    switch(inKey)
    {
        case 56: m_pose_xyz[0]+=0.05; std::cout<<"X+"; break;//8
        case 53: m_pose_xyz[0]-=0.05; std::cout<<"X-"; break;//5
        case 52: m_pose_xyz[1]-=0.05; std::cout<<"Y-"; break;//4
        case 54: m_pose_xyz[1]+=0.05; std::cout<<"Y+"; break;//6
        case 119: m_pose_xyz[2]+=0.05; std::cout<<"Z+"; break;//w
        case 115: m_pose_xyz[2]-=0.05; std::cout<<"Z-"; break;//s
        case 97: m_pose_rpy[2]+=0.087266459; std::cout<<"Yaw+"; break;//a
        case 100: m_pose_rpy[2]-=0.087266459; std::cout<<"Yaw-"; break;//d
        case 255: popMarker();std::cout<<"Undo"; break;//DEL
        case 13: placeMarker(); std::cout<<"Place"; std::cout<<m_pose_rpy[2];
      if(logFile != NULL){
        fprintf(logFile, "%lf\t\t%lf\t\t%lf\t\t%lf\n", m_pose_xyz[0], m_pose_xyz[1], m_pose_xyz[2], m_pose_rpy[2]);
        std::cout<<"saved";
        }
      break;//Enter
        default :  std::cout<<"Wrong (" <<inKey<<")"<<std::endl;; return;
    }
    pub_current_marker();
    std::cout<<std::endl;
}

void waypoint_picker::pub_current_marker()
{
    Marker_now.header.frame_id = "camera_init";
    Marker_now.header.stamp = ros::Time::now();
    Marker_now.ns = "curpos";
    Marker_now.id = 0;
    Marker_now.type = visualization_msgs::Marker::ARROW;
    Marker_now.action = visualization_msgs::Marker::ADD;
    Marker_now.pose.position.x = m_pose_xyz[0];
    Marker_now.pose.position.y = m_pose_xyz[1];
    Marker_now.pose.position.z = m_pose_xyz[2];
    Eigen::Quaternionf q;
    q = Eigen::AngleAxisf(m_pose_rpy[0], Eigen::Vector3f::UnitX())
        * Eigen::AngleAxisf(m_pose_rpy[1], Eigen::Vector3f::UnitY())
        * Eigen::AngleAxisf(m_pose_rpy[2], Eigen::Vector3f::UnitZ());
    Marker_now.pose.orientation.x = q.coeffs().coeff(0);
    Marker_now.pose.orientation.y = q.coeffs().coeff(1);
    Marker_now.pose.orientation.z = q.coeffs().coeff(2);
    Marker_now.pose.orientation.w = q.coeffs().coeff(3);

    Marker_now.scale.x = 0.5;
    Marker_now.scale.y = 0.25;
    Marker_now.scale.z = 0.25;

    Marker_now.color.r = 0.0f;
    Marker_now.color.g = 1.0f;
    Marker_now.color.b = 0.0f;
    Marker_now.color.a = 1.0;

    m_pub_marker_now.publish(Marker_now);


}

void waypoint_picker::popMarker()
{
    if(m_marker_array.size()!=0)
    {
        visualization_msgs::Marker marker_erase = m_marker_array.at(m_marker_array.size()-1);
        marker_erase.action = visualization_msgs::Marker::DELETE;
        m_marker_array.pop_back();
        visualization_msgs::MarkerArray Marker_array_msg;
        Marker_array_msg.markers = m_marker_array;
        Marker_array_msg.markers.push_back(marker_erase);
        m_pub_waypoints.publish(Marker_array_msg);
     }
}


void waypoint_picker::placeMarker()
{
    visualization_msgs::Marker marker_now_edit;
    marker_now_edit = Marker_now;
    marker_now_edit.id = m_marker_array.size();
    marker_now_edit.color.r = 1.0f;
    marker_now_edit.color.b = 0.0f;
    marker_now_edit.color.g = 0.0f;
    m_marker_array.push_back(marker_now_edit);
    visualization_msgs::MarkerArray Marker_array_msg;
    Marker_array_msg.markers = m_marker_array;
    m_pub_waypoints.publish(Marker_array_msg);
}
