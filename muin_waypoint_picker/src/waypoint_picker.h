/*#ifndef WAYPOINTPICKERCLASS_H
#define WAYPOINTPICKERCLASS_H

#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <eigen3/Eigen/Dense>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

namespace elvis
{
    class waypoint_picker // : private xtion
    {
    private:
      ros::NodeHandle m_nh;
      ros::Publisher m_pub_marker_now;
      ros::Publisher m_pub_waypoints;
      std::vector<visualization_msgs::Marker> m_marker_array;
      Eigen::Vector3f m_pose_xyz;
      Eigen::Vector3f m_pose_rpy;
      visualization_msgs::Marker Marker_now;
      void placeMarker();
      void popMarker();
      void pub_current_marker();

    public:
        waypoint_picker();
        ~waypoint_picker();
        void spinOnce();
    };
}

#endif

*/
#ifndef WAYPOINTPICKERCLASS_H
#define WAYPOINTPICKERCLASS_H
#include <iostream>
#include <fstream>
#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <eigen3/Eigen/Dense>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
using namespace std;

namespace elvis
{
    class waypoint_picker // : private xtion
    {
    private:
      ros::NodeHandle m_nh;
      ros::Publisher m_pub_marker_now;
      ros::Publisher m_pub_waypoints;
      std::vector<visualization_msgs::Marker> m_marker_array;
      Eigen::Vector3f m_pose_xyz;
      Eigen::Vector3f m_pose_rpy;
      visualization_msgs::Marker Marker_now;
      void placeMarker();
      void popMarker();
      void pub_current_marker();

    public:
      FILE *logFile;
      waypoint_picker();
      ~waypoint_picker();
      void spinOnce();
    };
}

#endif













