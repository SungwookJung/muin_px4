#include "ros/ros.h"
#include "src/waypoint_picker.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "gp2d");
    elvis::waypoint_picker waypoint_picker_class = elvis::waypoint_picker();

    ros::Rate loop_rate(100);
    while (ros::ok())
    {
        ros::spinOnce();
        waypoint_picker_class.spinOnce();
    }

    ros::spin();

    return 0;
}
