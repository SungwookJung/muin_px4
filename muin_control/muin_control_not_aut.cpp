#include <ros/ros.h>
#include "./src/fncs_header.hpp"


int main(int argc, char **argv)
{
  ros::init(argc, argv, "muin_px4");
  ROS_INFO("muin_px4, Start~ :)");
  muin_control_class * muin_px4ler = new muin_control_class();

  muin_px4ler->nh.getParam("wp_vel",muin_px4ler->wp_vel);
  muin_px4ler->nh.getParam("height_vel",muin_px4ler->height_vel);
  muin_px4ler->nh.getParam("landing_vel",muin_px4ler->landing_vel);
  muin_px4ler->nh.getParam("yaw_vel",muin_px4ler->yaw_vel);//rad
  muin_px4ler->nh.getParam("take_off_height",muin_px4ler->take_off_height);
  muin_px4ler->nh.getParam("threshold_distance",muin_px4ler->threshold_distance);
  muin_px4ler->nh.getParam("arrive_distance",muin_px4ler->arrive_distance);
  muin_px4ler->nh.getParam("threshold_yaw",muin_px4ler->threshold_yaw); // degree
  muin_px4ler->nh.getParam("LOS_radius",muin_px4ler->LOS_radius);


  ros::Rate loop_rate(50);

  while(ros::ok()){
    ros::spinOnce();
    muin_px4ler->spinOnce();
    loop_rate.sleep();
  }
  return 0;
}
