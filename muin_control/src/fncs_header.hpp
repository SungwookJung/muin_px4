/*
 *
 */

#define not_ready_mode -1
#define standby_mode 0
#define take_off_mode 1
#define landing_mode 2
#define move_wp_mode 3
#define hover_wp_mode 4
#define automatic_wp_mode 5
#define pause_mode 6
#define manual_control_mode 7
#define return_home_mode 8
#define emergency_mode 9

#include <ros/ros.h>
#include <time.h>
#include <cmath>
#include <tf/tf.h>
#include <tf/transform_datatypes.h>

//Pix message
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/PositionTarget.h>
#include <nav_msgs/Odometry.h>

#include <std_msgs/Time.h>
#include <std_msgs/Float64.h>
#include "std_msgs/String.h"
#include "std_msgs/UInt16.h"

//UAV control message
#include "muin_px4/log_data.h"

//UAV control service
#include "muin_px4/take_off.h"
#include "muin_px4/landing.h"
#include "muin_px4/next_mission.h"
#include "muin_px4/previous_mission.h"
#include "muin_px4/pause_mission.h"
#include "muin_px4/emergency_landing.h"
#include "muin_px4/return_home.h"
#include "muin_px4/manual_control.h"
#include "muin_px4/automatic_mission_start.h"

// UAV Mission Service
#include "muin_px4/linenumin_waypointout.h"
#include "muin_px4/ui_mission_request.h"


class muin_control_class
{
private:

  //Pix pub
  ros::Publisher setpoint_raw_pub;

  //UAV pub
  ros::Publisher pub_log_data;


  //Pix sub
  ros::Subscriber sub_pix_state;
  ros::Subscriber sub_pix_poseLocal;


  //Pix service client
  ros::ServiceClient client_cmdArming;
  ros::ServiceClient set_mode_client;

  //UAV service client : read wp line
  ros::ServiceClient client_request_waypoint;


  //UAV service server
  ros::ServiceServer srv_take_off;
  ros::ServiceServer srv_landing;
  ros::ServiceServer srv_send_mission_info;
  ros::ServiceServer srv_automatic_mission_start;
  ros::ServiceServer srv_next_mission;
  ros::ServiceServer srv_previous_mission;
  ros::ServiceServer srv_pause_mission;
  ros::ServiceServer srv_emergency_landing;
  ros::ServiceServer srv_manual_control;
  ros::ServiceServer srv_return_home;


  //UAV service server
  bool fn_take_off(muin_px4::take_off::Request &req,
                   muin_px4::take_off::Response &res);

  bool fn_landing(muin_px4::landing::Request &req,
                  muin_px4::landing::Response &res);

  bool fn_send_mission_info(muin_px4::ui_mission_request::Request &req,
                            muin_px4::ui_mission_request::Response &res);

  bool fn_automatic_mission_start(muin_px4::automatic_mission_start::Request &req,
                                  muin_px4::automatic_mission_start::Response &res);

  bool fn_next_mission(muin_px4::next_mission::Request &req,
                       muin_px4::next_mission::Response &res);

  bool fn_previous_mission(muin_px4::previous_mission::Request &req,
                           muin_px4::previous_mission::Response &res);

  bool fn_pause_mission(muin_px4::pause_mission::Request &req,
                        muin_px4::pause_mission::Response &res);

  bool fn_manual_control(muin_px4::manual_control::Request &req,
                        muin_px4::manual_control::Response &res);

  bool fn_return_home(muin_px4::return_home::Request &req,
                      muin_px4::return_home::Response &res);

  bool fn_emergency_landing(muin_px4::emergency_landing::Request &req,
                            muin_px4::emergency_landing::Response &res);



  //UAV variable
    //state representing variable
  mavros_msgs::SetMode offb_set_mode; // for OFFBOARD
  ros::Time last_request; // for loop time
  ros::Time landing_time; // for landing loop time
  ros::Time arrive_time;

  bool flagOffboard;
  bool log_cnt;
  geometry_msgs::Point start_point;
  float start_yaw;
  float xy_dist;
  float xyz_dist;
  float xyz_total_dist;
  int manual_direction;
  double manual_distance;

    /*
     * UAV_control_mode
     * * not ready         : -1
     * * standby           :  0
     * * take off          :  1
     * * landing           :  2
     * * move to waypoint  :  3
     * * hover on waypoint :  4
     * * pause             :  5
     * * return home       :  6
     * * emergency         :  7
     */

    //Pix state/position/velocity/target
    mavros_msgs::State current_state;
    nav_msgs::Odometry pix_poseLocal;
    geometry_msgs::Quaternion current_orientation;
    geometry_msgs::Point start_wp;
    geometry_msgs::Point end_wp;
    mavros_msgs::PositionTarget pause_target;
    mavros_msgs::PositionTarget manual_target;
    mavros_msgs::PositionTarget target_position;
    geometry_msgs::Vector3 leftover;
    geometry_msgs::Vector3 total_distance;

  double landing_cur; // current altitude in landing seq.
  double landing_old; // previous altitude in landing seq.

  bool armed_flag;
  int UAV_control_mode;

    int velocity_control_type = mavros_msgs::PositionTarget::IGNORE_AFX|
        mavros_msgs::PositionTarget::IGNORE_AFY|
        mavros_msgs::PositionTarget::IGNORE_AFZ|
        mavros_msgs::PositionTarget::IGNORE_PX|
        mavros_msgs::PositionTarget::IGNORE_PY|
        mavros_msgs::PositionTarget::IGNORE_PZ|
        mavros_msgs::PositionTarget::IGNORE_YAW;

    int position_control_type = mavros_msgs::PositionTarget::IGNORE_AFX|
        mavros_msgs::PositionTarget::IGNORE_AFY|
        mavros_msgs::PositionTarget::IGNORE_AFZ|
        mavros_msgs::PositionTarget::IGNORE_VX|
        mavros_msgs::PositionTarget::IGNORE_VY|
        mavros_msgs::PositionTarget::IGNORE_VZ|
        mavros_msgs::PositionTarget::IGNORE_YAW_RATE;

    //UAV log
    muin_px4::log_data pix2muin_log_data;

    //ui_mission_request
    int wp_size;
    std::vector <geometry_msgs::Point> muin_wp_local;
    std::vector <double> muin_heading;

    //automatic_mission_start
    bool auto_flag;
    bool auto_arrive_cnt;

    //mission
    int wp_idx = -1;
    //pause_mode
    bool pause_toggle;

    void cb_pix_state(const mavros_msgs::State::ConstPtr& msg);
    void cb_pix_local(const nav_msgs::Odometry::ConstPtr &msg);



public:
ros::NodeHandle nh;

void spinOnce();

geometry_msgs::Vector3 Quat2Angle(geometry_msgs::Quaternion quat);
geometry_msgs::Vector3 euclidean_xyz_distance(geometry_msgs::Point start, geometry_msgs::Point end);
float yaw_distance(float start, float end);

void set_start_end_wp(int idx);
void move();

//velocity & take off height & threshold for detecting arrive
double wp_vel;
double height_vel;
double landing_vel;
double yaw_vel;
double take_off_height;
double threshold_distance;
double arrive_distance;
double threshold_yaw;
double LOS_radius;
double auto_arrive_delay;

muin_control_class()
{
  setpoint_raw_pub = nh.advertise<mavros_msgs::PositionTarget>("/mavros/setpoint_raw/local", 10);

  pub_log_data = nh.advertise<muin_px4::log_data>("/muin/log_data", 30);


  sub_pix_state = nh.subscribe<mavros_msgs::State>("/mavros/state", 10, &muin_control_class::cb_pix_state,this);
  sub_pix_poseLocal = nh.subscribe<nav_msgs::Odometry>("/mavros/local_position/odom", 1, &muin_control_class::cb_pix_local,this);


  //Pix service client
  client_cmdArming = nh.serviceClient<mavros_msgs::CommandBool>("/mavros/cmd/arming");
  set_mode_client = nh.serviceClient<mavros_msgs::SetMode>("/mavros/set_mode");

  //UAV service client : read wp line
  client_request_waypoint = nh.serviceClient<muin_px4::linenumin_waypointout>("/wp_reader/request");


  //UAV service server
  srv_take_off = nh.advertiseService("take_off", &muin_control_class::fn_take_off,this);
  srv_landing = nh.advertiseService("landing", &muin_control_class::fn_landing,this);
  srv_send_mission_info = nh.advertiseService("send_mission_info", &muin_control_class::fn_send_mission_info,this);
  srv_automatic_mission_start = nh.advertiseService("automatic_mission_start", &muin_control_class::fn_automatic_mission_start,this);
  srv_next_mission = nh.advertiseService("next_mission", &muin_control_class::fn_next_mission,this);
  srv_previous_mission = nh.advertiseService("previous_mission", &muin_control_class::fn_previous_mission,this);
  srv_pause_mission = nh.advertiseService("pause_mission", &muin_control_class::fn_pause_mission,this);
  srv_emergency_landing = nh.advertiseService("emergency_landing", &muin_control_class::fn_emergency_landing,this);
  srv_manual_control = nh.advertiseService("manual_control", &muin_control_class::fn_manual_control,this);
  srv_return_home = nh.advertiseService("return_home", &muin_control_class::fn_return_home,this);

  armed_flag = false;
  UAV_control_mode = not_ready_mode;
  target_position.coordinate_frame = mavros_msgs::PositionTarget::FRAME_LOCAL_NED;
  pause_toggle = 0;
  last_request = ros::Time::now();
  offb_set_mode.request.custom_mode = "OFFBOARD";
  pause_toggle = false;
  log_cnt = false;

  auto_flag = false;
  auto_arrive_cnt = false;

}
~muin_control_class()
{

}
};
