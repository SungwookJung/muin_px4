#include "fncs_header.hpp"

//UAV service server
bool muin_control_class::fn_take_off(muin_px4::take_off::Request &req,
                 muin_px4::take_off::Response &res){
    pause_toggle = false;
  if(!armed_flag){
    flagOffboard = true;
    if(muin_wp_local.size() > 0 && UAV_control_mode == standby_mode)
    {
      target_position.type_mask = position_control_type;
      target_position.position.x = 0;
      target_position.position.y = 0;
      target_position.position.z = take_off_height;
      target_position.yaw = Quat2Angle(current_orientation).z;
      setpoint_raw_pub.publish(target_position);
      UAV_control_mode = take_off_mode;

      // log
      pix2muin_log_data.string_color = 1;
      pix2muin_log_data.log_string = "TakeOff wait";
      ROS_INFO("TakeOff wait");
      pub_log_data.publish(pix2muin_log_data);

      res.result = true;
      return true;
    }
    else{
        UAV_control_mode = not_ready_mode;

        // log
        pix2muin_log_data.string_color = 1;
        pix2muin_log_data.log_string = "Cannot takeoff, check waypoint & UAV state";
        ROS_ERROR("Cannot takeoff, check waypoint & UAV state");
        pub_log_data.publish(pix2muin_log_data);

        res.result =false;
        return false;
    }
  }
  else{
    // log
    pix2muin_log_data.string_color = 0;
    pix2muin_log_data.log_string = "Maybe already finished TakeOff";
    ROS_ERROR("Maybe already finished TakeOff");
    pub_log_data.publish(pix2muin_log_data);

    res.result =false;
    return false;
  }
}

bool muin_control_class::fn_landing(muin_px4::landing::Request &req,
                muin_px4::landing::Response &res){
  UAV_control_mode = landing_mode;
  res.result = true;
}

bool muin_control_class::fn_send_mission_info(muin_px4::ui_mission_request::Request &req,
                          muin_px4::ui_mission_request::Response &res){
  if(current_state.system_status == 3 && !armed_flag){
    muin_px4::linenumin_waypointout data;
    data.request.line_num = 100;
    client_request_waypoint.call(data);
    wp_size = data.response.total_line + 1;
    ROS_INFO("total line : %d", wp_size);
    muin_wp_local.clear();
    muin_heading.clear();
    res.complete = true;
    if(wp_size>0){
      muin_wp_local.resize(wp_size);
      muin_heading.resize(wp_size);
      bool take_wp;
      for (int line = 0;line < wp_size;line++) {
        data.request.line_num = line;
        client_request_waypoint.call(data);
        take_wp = data.response.exist;
        if (take_wp){
          muin_wp_local[line] = data.response.waypoint.position;
          muin_heading[line] = data.response.waypoint.orientation.w;
          if(line == wp_size-1){
            pix2muin_log_data.string_color = 2;
            pix2muin_log_data.log_string = "There is mission";
            ROS_INFO("There is %d mission",wp_size);
            pub_log_data.publish(pix2muin_log_data);

            UAV_control_mode = standby_mode;
          }
        }
        else{
          line = wp_size;
          pix2muin_log_data.string_color = 1;
          pix2muin_log_data.log_string = "Something wrong with waypoint request!";
          ROS_ERROR("Something wrong with waypoint request!");
          pub_log_data.publish(pix2muin_log_data);

          res.complete = false;
        }
      }
    }
    else {
      pix2muin_log_data.string_color = 1;
      pix2muin_log_data.log_string = "There is no mission, please check mission";
      ROS_ERROR("There is no mission, please check mission");
      pub_log_data.publish(pix2muin_log_data);
      res.complete = false;
    }
  }
  else{
    pix2muin_log_data.string_color = 0;
    pix2muin_log_data.log_string = "Not a proper state for upload, please not insert mission";
    ROS_ERROR("Not a proper state for upload, please not insert mission");
    pub_log_data.publish(pix2muin_log_data);
    res.complete =false;
  }
  return res.complete;
}

bool muin_control_class::fn_automatic_mission_start(muin_px4::automatic_mission_start::Request &req,
                                                   muin_px4::automatic_mission_start::Response &res){
    UAV_control_mode = automatic_wp_mode;
    auto_flag = true;
    res.result = true;
}


bool muin_control_class::fn_next_mission(muin_px4::next_mission::Request &req,
                     muin_px4::next_mission::Response &res){
  pause_toggle = false;
  log_cnt = true;
  wp_idx++;
  if (wp_idx<wp_size){
    UAV_control_mode = move_wp_mode;
    // log
    pix2muin_log_data.string_color = 0;
    pix2muin_log_data.log_string = "Command for next waypoint received";
    ROS_INFO("Command for next waypoint received");
    pub_log_data.publish(pix2muin_log_data);
    res.result = true;
  }
  else {
    wp_idx--;
    UAV_control_mode = hover_wp_mode;
    // log
    pix2muin_log_data.string_color = 0;
    pix2muin_log_data.log_string = "This is the last mission, cannot go to next waypoint";
    ROS_INFO("This is the last mission, cannot go to next waypoint");
    pub_log_data.publish(pix2muin_log_data);
    res.result = false;
  }
}

bool muin_control_class::fn_previous_mission(muin_px4::previous_mission::Request &req,
                         muin_px4::previous_mission::Response &res){
  pause_toggle = false;
  log_cnt = true;
  wp_idx--;
  if (wp_idx<0){
    wp_idx++;
    UAV_control_mode = hover_wp_mode;
    // log
    pix2muin_log_data.string_color = 0;
    pix2muin_log_data.log_string = "This is the first waypoint, cannot go to previous waypoint";
    pub_log_data.publish(pix2muin_log_data);
    res.result = false;
  }
  else {
    UAV_control_mode = move_wp_mode;
    // log
    pix2muin_log_data.string_color = 0;
    pix2muin_log_data.log_string = "Command for previous waypoint received";
    pub_log_data.publish(pix2muin_log_data);
    res.result = true;
  }
}

bool muin_control_class::fn_pause_mission(muin_px4::pause_mission::Request &req,
                      muin_px4::pause_mission::Response &res){
  UAV_control_mode = pause_mode;
  pause_toggle =!pause_toggle;
  res.result = true;
}

bool muin_control_class::fn_manual_control(muin_px4::manual_control::Request &req,
                                        muin_px4::manual_control::Response &res){

    manual_direction = req.direction;
    manual_distance = req.distance;

    manual_target.position = pix_poseLocal.pose.pose.position;
    manual_target.yaw = Quat2Angle(current_orientation).z;
    manual_target.velocity.x = 0;
    manual_target.velocity.y = 0;
    manual_target.velocity.z = 0;
    manual_target.yaw_rate = 0;

    switch (manual_direction){
        case muin_px4::manual_control::Request::X_PLUS:
        {
            ROS_INFO("X_PLUS");
            manual_target.position.x += manual_distance;
            manual_target.velocity.x += wp_vel;
        }
            break;
        case muin_px4::manual_control::Request::X_MINUS:
        {
            ROS_INFO("X_MINUS");
            manual_target.position.x -= manual_distance;
            manual_target.velocity.x -= wp_vel;
        }
            break;
        case muin_px4::manual_control::Request::Y_PLUS:
        {
            ROS_INFO("Y_PLUS");
            manual_target.position.y += manual_distance;
            manual_target.velocity.y += wp_vel;
        }
            break;
        case muin_px4::manual_control::Request::Y_MINUS:
        {
            ROS_INFO("Y_MINUS");
            manual_target.position.y -= manual_distance;
            manual_target.velocity.y -= wp_vel;
        }
            break;
        case muin_px4::manual_control::Request::Z_PLUS:
        {
            ROS_INFO("Z_PLUS");
            manual_target.position.z += manual_distance;
            manual_target.velocity.z += wp_vel;
        }
            break;
        case muin_px4::manual_control::Request::Z_MINUS:
        {
            ROS_INFO("Z_MINUS");
            manual_target.position.z -= manual_distance;
            manual_target.velocity.z -= wp_vel;
        }
            break;
        case muin_px4::manual_control::Request::YAW_PLUS:
        {
            ROS_INFO("YAW_PLUS");
            manual_target.yaw += manual_distance;
            manual_target.yaw_rate += yaw_vel;
        }
            break;
        case muin_px4::manual_control::Request::YAW_MINUS:
        {
            ROS_INFO("YAW_MINUS");
            manual_target.yaw -= manual_distance;
            manual_target.yaw_rate -= yaw_vel;
        }
            break;
    }

    UAV_control_mode = manual_control_mode;
    res.result = true;
}

bool muin_control_class::fn_return_home(muin_px4::return_home::Request &req,
                    muin_px4::return_home::Response &res){
  UAV_control_mode = return_home_mode;
  res.result = true;
}

bool muin_control_class::fn_emergency_landing(muin_px4::emergency_landing::Request &req,
                          muin_px4::emergency_landing::Response &res){
  UAV_control_mode = emergency_mode;
  res.result = true;
}

