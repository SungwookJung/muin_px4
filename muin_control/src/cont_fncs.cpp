#include "fncs_header.hpp"

void muin_control_class::spinOnce(){
    if(!armed_flag){
        start_point = pix_poseLocal.pose.pose.position;
        start_yaw = muin_control_class::Quat2Angle(current_orientation).z;
    }
    if(!pause_toggle){
        pause_target.position = pix_poseLocal.pose.pose.position;
        pause_target.yaw = muin_control_class::Quat2Angle(current_orientation).z;
    }
//  ROS_INFO("mode %d",UAV_control_mode);

  if(UAV_control_mode == emergency_mode){
    target_position.type_mask = velocity_control_type;
    target_position.yaw_rate = 0;
    target_position.velocity.x = 0;
    target_position.velocity.y = 0;
    target_position.velocity.z = -landing_vel;
    setpoint_raw_pub.publish(target_position);
  }
  else {
    target_position.velocity.x = 0;
    target_position.velocity.y = 0;
    target_position.velocity.z = 0;
    target_position.yaw_rate = 0;
    target_position.position.x = 0;
    target_position.position.y = 0;
    target_position.position.z = 0;
    target_position.yaw = 0;
      switch (UAV_control_mode) {
          case take_off_mode:
          {
              target_position.type_mask = position_control_type;
              target_position.position.x = start_point.x;
              target_position.position.y = start_point.y;
              target_position.position.z = take_off_height;
              target_position.yaw = Quat2Angle(current_orientation).z;

              mavros_msgs::CommandBool cmd;
              cmd.request.value = true;

              setpoint_raw_pub.publish(target_position);

              if(!armed_flag){

                  client_cmdArming.call(cmd);

                  // log
                  pix2muin_log_data.string_color = 1;
                  pix2muin_log_data.log_string = "Not armed";
                  ROS_INFO("Not armed");
                  pub_log_data.publish(pix2muin_log_data);
              }

              if(flagOffboard){
                  if(current_state.mode != "OFFBOARD" && (ros::Time::now() - last_request > ros::Duration(1.0))){

                      if(set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent){
                          ROS_INFO("Offboard enabled");
                          pix2muin_log_data.string_color = 1;
                          pix2muin_log_data.log_string = "Offboard enabled";
                          pub_log_data.publish(pix2muin_log_data);
                      }
                      last_request = ros::Time::now();
                  }
                  if(abs(pix_poseLocal.pose.pose.position.z - take_off_height) < threshold_distance){
                      flagOffboard = true;
                      wp_idx = -1;
                      UAV_control_mode = hover_wp_mode;
                  }
              }
          }
              break;
          case landing_mode:
          {
              auto_flag = false;
              target_position.type_mask = velocity_control_type;
              target_position.velocity.z = (-1)*landing_vel;
              if(ros::Time::now() - landing_time > ros::Duration(4.0)){
                  landing_old = landing_cur; // Change old height value
                  landing_cur = pix_poseLocal.pose.pose.position.z; // Change current height value
                  landing_time = ros::Time::now();
                  printf("old : %lf // cur : %lf // delta : %lf \n",landing_old, landing_cur, landing_cur - landing_old);

                  // landing clear condition
                  // old height value > cur height value and the difference is less than 50cm
                  if((landing_old > landing_cur) && abs(landing_old - landing_cur) < 0.5){
                      target_position.type_mask = position_control_type;
                      target_position.velocity.z = 0;
                      target_position.position.x = pix_poseLocal.pose.pose.position.x;
                      target_position.position.y = pix_poseLocal.pose.pose.position.y;
                      target_position.position.z = 0;

                      mavros_msgs::CommandBool cmd;
                      cmd.request.value = false;
                      client_cmdArming.call(cmd);

                      if(!armed_flag){
                          // log
                          pix2muin_log_data.string_color = 0;
                          pix2muin_log_data.log_string = "Landing clear";
                          pub_log_data.publish(pix2muin_log_data);

                          UAV_control_mode = not_ready_mode;
                          wp_idx = -1;
                          wp_size = 0;
                          landing_old = 0;
                      }
                      else {
                          // log
                          pix2muin_log_data.string_color = 0;
                          pix2muin_log_data.log_string = "Landing on going";
                          pub_log_data.publish(pix2muin_log_data);

                          UAV_control_mode = landing_mode;
                      }
                  }
              }
              setpoint_raw_pub.publish(target_position);
          }
              break;
          case pause_mode:
          {
              target_position.type_mask = position_control_type;
              target_position.position = pause_target.position;
              target_position.yaw = pause_target.yaw;
              setpoint_raw_pub.publish(target_position);
              // log
              pix2muin_log_data.string_color = 0;
              pix2muin_log_data.log_string = "UAV paused";
              ROS_INFO("UAV paused");
              pub_log_data.publish(pix2muin_log_data);

              if(!pause_toggle){
                  UAV_control_mode = move_wp_mode*(!auto_flag)+automatic_wp_mode*(auto_flag);
              }

              setpoint_raw_pub.publish(target_position);
          }
              break;
          case return_home_mode:
          {
              target_position.type_mask = position_control_type;
              target_position.position = start_point;
              target_position.yaw = start_yaw;
              // log
              pix2muin_log_data.string_color = 0;
              pix2muin_log_data.log_string = "Returning home";
              pub_log_data.publish(pix2muin_log_data);
              setpoint_raw_pub.publish(target_position);
          }
              break;
          default:
          {
              if (auto_flag){
                  switch (UAV_control_mode){
                      case (automatic_wp_mode):
                          {
                          if (wp_idx == -1) wp_idx = 0;
                          move();
                          setpoint_raw_pub.publish(target_position);
                          auto_arrive_cnt = false;
                      }
                      break;
                      case hover_wp_mode:
                      {
                          if(log_cnt){
                              // log
                              pix2muin_log_data.string_color = 0;
                              pix2muin_log_data.log_string = "Hovering mode activated";
                              ROS_INFO("Hovering waypoint : %d", wp_idx);
                              pub_log_data.publish(pix2muin_log_data);
                              log_cnt = false;
                          }

                          target_position.type_mask = position_control_type;
                          if(wp_idx >= 0 && wp_idx < wp_size){
                              target_position.position = muin_wp_local[wp_idx];
                              target_position.yaw = muin_control_class::Quat2Angle(current_orientation).z;

                              if(xy_dist < arrive_distance && abs(leftover.z) < arrive_distance){
                                  target_position.position = muin_wp_local[wp_idx];
                                  target_position.yaw = muin_heading[wp_idx];

                                  if (!auto_arrive_cnt){
                                      arrive_time = ros::Time::now();
                                      auto_arrive_cnt = true;

                                      // log
                                      pix2muin_log_data.string_color = 0;
                                      pix2muin_log_data.log_string = "Waypoint arrive!";
                                      ROS_INFO("Waypoint arrive!");
                                      pub_log_data.publish(pix2muin_log_data);
                                  }

                                  if(ros::Time::now() - arrive_time > ros::Duration(auto_arrive_delay)){
                                      wp_idx++;
                                      UAV_control_mode = automatic_wp_mode;
                                      arrive_time = ros::Time::now();

                                      // log
                                      pix2muin_log_data.string_color = 0;
                                      pix2muin_log_data.log_string = "Going next!";
                                      ROS_INFO("Going next!");
                                      pub_log_data.publish(pix2muin_log_data);
                                  }
                              }
                          }
                          else {
                              target_position.type_mask = position_control_type;
                              target_position.position.x = start_point.x;
                              target_position.position.y = start_point.y;
                              target_position.position.z = take_off_height;
                              target_position.yaw = muin_control_class::Quat2Angle(current_orientation).z;
                              // log
                              pix2muin_log_data.string_color = 0;
                              pix2muin_log_data.log_string = "Take off clear!";
                              ROS_INFO("Take off clear!");
                              pub_log_data.publish(pix2muin_log_data);
                          }
                          setpoint_raw_pub.publish(target_position);
                      }
                          break;

                      default :
                          {
                          // log
                          pix2muin_log_data.string_color = 0;
                          pix2muin_log_data.log_string = "Cannot choose, auto mode activated!";
                          pub_log_data.publish(pix2muin_log_data);
                          }
                          break;
                  }
              }
              else{
                  switch (UAV_control_mode){
                  case move_wp_mode:
                  {
                      move();
                      setpoint_raw_pub.publish(target_position);
                  }
                  break;
                  case hover_wp_mode:
                  {
                      if(log_cnt){
                          // log
                          pix2muin_log_data.string_color = 0;
                          pix2muin_log_data.log_string = "Hovering mode activated";
                          ROS_INFO("Hovering waypoint : %d", wp_idx);
                          pub_log_data.publish(pix2muin_log_data);
                          log_cnt = false;
                      }

                      target_position.type_mask = position_control_type;
                      if(wp_idx >= 0 && wp_idx < wp_size){
                          target_position.position = muin_wp_local[wp_idx];
                          target_position.yaw = muin_control_class::Quat2Angle(current_orientation).z;

                          if(xy_dist < arrive_distance && abs(leftover.z) < arrive_distance){
                              target_position.position = muin_wp_local[wp_idx];
                              target_position.yaw = muin_heading[wp_idx];
                              // log
                              pix2muin_log_data.string_color = 0;
                              pix2muin_log_data.log_string = "Waypoint arrive!";
                              ROS_INFO("Waypoint arrive!");
                              pub_log_data.publish(pix2muin_log_data);
                          }
                          else{
                              // log
                              pix2muin_log_data.string_color = 0;
                              pix2muin_log_data.log_string = "Waypoint arrive, Heading align needed";
                              ROS_INFO("Waypoint arrive, Heading align needed");
                              pub_log_data.publish(pix2muin_log_data);
                          }
                      }
                      else {
                          target_position.type_mask = position_control_type;
                          target_position.position.x = start_point.x;
                          target_position.position.y = start_point.y;
                          target_position.position.z = take_off_height;
                          target_position.yaw = muin_control_class::Quat2Angle(current_orientation).z;
                          // log
                          pix2muin_log_data.string_color = 0;
                          pix2muin_log_data.log_string = "Take off clear!";
                          ROS_INFO("Take off clear!");
                          pub_log_data.publish(pix2muin_log_data);
                      }
                      setpoint_raw_pub.publish(target_position);
                  }
                  break;
                      case manual_control_mode:
                      {
                          geometry_msgs::Vector3 manual_leftover = euclidean_xyz_distance(pix_poseLocal.pose.pose.position, manual_target.position);
                          float manual_yaw_leftover = yaw_distance(Quat2Angle(current_orientation).z, manual_target.yaw);

                          target_position = manual_target;

                          if (((manual_leftover.x*(manual_direction<muin_px4::manual_control::Request::Y_PLUS)+manual_leftover.y*(manual_direction<muin_px4::manual_control::Request::Z_PLUS && manual_direction>muin_px4::manual_control::Request::X_MINUS)+manual_leftover.y*(manual_direction<muin_px4::manual_control::Request::YAW_PLUS && manual_direction>muin_px4::manual_control::Request::Y_MINUS))<threshold_distance)*(manual_direction<muin_px4::manual_control::Request::YAW_PLUS)||(manual_direction>muin_px4::manual_control::Request::Z_MINUS)){
                              target_position.type_mask = position_control_type;
                          }
                          else{
                              manual_distance -= (wp_vel*(manual_direction<muin_px4::manual_control::Request::YAW_PLUS))/50.0;
                              ROS_INFO("%lf",manual_distance);
                              target_position.type_mask = velocity_control_type;
                          }
                          setpoint_raw_pub.publish(target_position);
                      }
                          break;
                  }
              }
          }
          break;
      }
  }
}
//easy function
geometry_msgs::Vector3 muin_control_class::Quat2Angle(geometry_msgs::Quaternion quat)
{
  geometry_msgs::Vector3 res;
  tf::Matrix3x3 R_FLU2ENU(tf::Quaternion(quat.x, quat.y, quat.z, quat.w));
  R_FLU2ENU.getRPY(res.x, res.y, res.z);
  return res;
}

geometry_msgs::Vector3 muin_control_class::euclidean_xyz_distance(geometry_msgs::Point start, geometry_msgs::Point end){
  geometry_msgs::Vector3 res;
  res.x = end.x - start.x;
  res.y = end.y - start.y;
  res.z = end.z - start.z;
  return res;
}

float muin_control_class::yaw_distance(float start, float end){
  return end - start;
}
void muin_control_class::set_start_end_wp(int idx){
    if(idx == 0){
        start_wp.x = 0;
        start_wp.y = 0;
        start_wp.z = take_off_height;
    }
    else{
        start_wp = muin_wp_local[idx-1];
    }
    end_wp = muin_wp_local[idx];
}

void muin_control_class::move(){
    target_position.type_mask = velocity_control_type;
    set_start_end_wp(wp_idx);
    geometry_msgs::Vector3 leftover = euclidean_xyz_distance(pix_poseLocal.pose.pose.position,muin_wp_local[wp_idx]);
    geometry_msgs::Vector3 total_distance = euclidean_xyz_distance(start_wp,end_wp);
    float xy_dist = sqrt(pow(leftover.x,2)+pow(leftover.y,2));
    float xyz_dist = sqrt(pow(leftover.x,2)+pow(leftover.y,2)+pow(leftover.z,2));
    float xyz_total_dist = sqrt(pow(total_distance.x,2)+pow(total_distance.y,2)+pow(total_distance.z,2));
    ROS_INFO("xy_dist : %lf",xy_dist);
    ROS_INFO("z axis : %lf",leftover.z);
    float yaw_dist = yaw_distance(Quat2Angle(current_orientation).z,muin_heading[wp_idx]);
    ROS_INFO("yaw_dist : %lf",yaw_dist);
    if(xy_dist < threshold_distance && abs(leftover.z) < threshold_distance){
        ROS_INFO("xy aligned!");
        target_position.type_mask = position_control_type;
        target_position.position = muin_wp_local[wp_idx];
        target_position.yaw = muin_heading[wp_idx];
        log_cnt = true;
        UAV_control_mode = hover_wp_mode;
    }
    else{
        float perpendicular_ratio;
        geometry_msgs::Point perpendicular_point;
        perpendicular_ratio= (total_distance.x * (pix_poseLocal.pose.pose.position.x - start_wp.x) + total_distance.y * (pix_poseLocal.pose.pose.position.y - start_wp.y) + total_distance.z * (pix_poseLocal.pose.pose.position.z - start_wp.z))/pow(xyz_total_dist,2);
        perpendicular_point.x = start_wp.x + total_distance.x * perpendicular_ratio;
        perpendicular_point.y = start_wp.y + total_distance.y * perpendicular_ratio;
        perpendicular_point.z = start_wp.z + total_distance.z * perpendicular_ratio;

        geometry_msgs::Vector3 perpendicular_dist;
        float perpendicular_total_dist;
        perpendicular_dist = euclidean_xyz_distance(pix_poseLocal.pose.pose.position,perpendicular_point);
        perpendicular_total_dist = sqrt(pow(perpendicular_dist.x,2)+pow(perpendicular_dist.y,2)+pow(perpendicular_dist.z,2));

        float final_ratio;
        if(perpendicular_total_dist<1 && perpendicular_total_dist>0){
            if(LOS_radius>perpendicular_total_dist) {
                final_ratio = perpendicular_ratio + sqrt(pow(LOS_radius,2)-pow(perpendicular_total_dist,2))/xyz_total_dist;
            }
            else {
                final_ratio = perpendicular_ratio;
            }
        }
        else{
            final_ratio = 1;
        }
        final_ratio = 1 + (final_ratio - 1)*((final_ratio - 1) < 0)-final_ratio*(final_ratio<0);

        geometry_msgs::Point local_destination;

        local_destination.x = start_wp.x + final_ratio*total_distance.x;
        local_destination.y = start_wp.y + final_ratio*total_distance.y;
        local_destination.z = start_wp.z + final_ratio*total_distance.z;

        geometry_msgs::Vector3 local_diff;
        local_diff = euclidean_xyz_distance(pix_poseLocal.pose.pose.position,local_destination);
        float local_dist = sqrt(pow(local_diff.x,2)+pow(local_diff.y,2)+pow(local_diff.z,2));

        target_position.velocity.x = local_diff.x*wp_vel/local_dist;
        target_position.velocity.y = local_diff.y*wp_vel/local_dist;
        target_position.velocity.z = local_diff.z*wp_vel/local_dist;
        target_position.yaw_rate = 0;
        ROS_INFO("local diff x : %lf",local_diff.x);
        ROS_INFO("local diff y : %lf",local_diff.y);
        ROS_INFO("local diff z : %lf",local_diff.z);
    }
}
