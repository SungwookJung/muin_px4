#include <ros/ros.h>
#include "./src/fncs_header.hpp"


int main(int argc, char **argv)
{
  ros::init(argc, argv, "muin_control");
  ROS_INFO("muin_control, Start~ :)");
  muin_control_class * uav_controller = new muin_control_class();

  uav_controller->nh.getParam("wp_vel",uav_controller->wp_vel);
  uav_controller->nh.getParam("height_vel",uav_controller->height_vel);
  uav_controller->nh.getParam("landing_vel",uav_controller->landing_vel);
  uav_controller->nh.getParam("yaw_vel",uav_controller->yaw_vel);//rad
  uav_controller->nh.getParam("take_off_height",uav_controller->take_off_height);
  uav_controller->nh.getParam("threshold_distance",uav_controller->threshold_distance);
  uav_controller->nh.getParam("arrive_distance",uav_controller->arrive_distance);
  uav_controller->nh.getParam("threshold_yaw",uav_controller->threshold_yaw); // degree
  uav_controller->nh.getParam("LOS_radius",uav_controller->LOS_radius);
  uav_controller->nh.getParam("auto_arrive_delay",uav_controller->auto_arrive_delay);

  ros::Rate loop_rate(50);

  while(ros::ok()){
    ros::spinOnce();
    uav_controller->spinOnce();
    loop_rate.sleep();
  }
  return 0;
}
