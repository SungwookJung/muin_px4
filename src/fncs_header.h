#include <ros/ros.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <fstream>

#include "diagnostic_msgs/DiagnosticStatus.h"
#include "diagnostic_msgs/DiagnosticArray.h"
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/BatteryState.h>
// pixhawk
#include <mavlink/config.h>
#include <mavros_msgs/AttitudeTarget.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/HomePosition.h>
#include <mavros_msgs/RCIn.h>
#include <mavros_msgs/RCOut.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/CommandHome.h>
#include <mavros_msgs/PositionTarget.h>
#include <mavros_msgs/BatteryStatus.h>

class muin
{
private:

  // pixhawk
  ros::Publisher m_pub_cmdVel;

  // pub data

  ros::Publisher m_pub_logdata;
  ros::Publisher m_pub_battinfo;


  // sub data
  ros::Subscriber m_sub_battery;
  ros::Subscriber m_sub_state ;
  ros::Subscriber m_sub_poseGloabl ;
  ros::Subscriber m_sub_poseLocal ;
  ros::Subscriber m_sub_homePosition ;
  ros::Subscriber m_sub_imu ;
  ros::Subscriber m_sub_diagnostic;


  //##################################//
  //#######  CALL BACK FUNCTION ######//
  //##################################//

  // for check diagnostic
  diagnostic_msgs::DiagnosticStatus m_diagnostic_GPS;
  diagnostic_msgs::DiagnosticStatus m_diagnostic_sensor;
  diagnostic_msgs::DiagnosticArray m_diagnostic;
  void cb_pix_diagnostic(const diagnostic_msgs::DiagnosticArray::ConstPtr &msg);

  // subscribe for battery
  sensor_msgs::BatteryState m_pixbattery;
  void cb_pix_battery(const sensor_msgs::BatteryState::ConstPtr &msg);

  mavros_msgs::State pix_state;
  void cb_pix_state(const mavros_msgs::State::ConstPtr &msg);


  //##################################//
  //#######     SERVICE LIST    ######//
  //##################################//

  ros::ServiceClient m_client_cmdOffBoard;
  ros::ServiceClient m_client_cmdArming;
  ros::ServiceClient m_client_cmdSetHome;

  ros::ServiceServer m_srv_take_off;
  ros::ServiceServer m_srv_return_home;
  ros::ServiceServer m_srv_request_mode_info;
  ros::ServiceServer m_srv_landing;
 // ros::ServiceServer m_srv_next_mission; // BORYU
 // ros::ServiceServer m_srv_previous_mission; // BORYU
  ros::ServiceServer m_srv_pause_mission;

  ros::ServiceServer m_srv_emergency_landing;
  ros::ServiceServer m_srv_request_gps_mission;
  ros::ServiceServer m_srv_send_mission_info;
  ros::ServiceServer m_srv_move_control;
  ros::ServiceServer m_srv_rotate_control;

// BORYU


}
