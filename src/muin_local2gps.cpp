//
// Created by duckyu-laptop on 19. 8. 16..
//

#include <ros/ros.h>
#include "../include/utm.h"
#include "muin_px4//start_trig.h"
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <mavros_msgs/HomePosition.h>
#include <mavros_msgs/CommandHome.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Bool.h>


sensor_msgs::NavSatFix GPS_data;
geometry_msgs::PoseStamped local_data;
mavros_msgs::HomePosition start_point;
sensor_msgs::NavSatFix estimated_data;
int estimated_data_seq = 0;
bool gps_en=false;
bool gps_en_past=false;
double local_x_start=0;
double local_y_start=0;
double local_z_start=0;
/*
void cb_m_start_trigger(const std_msgs::Bool::ConstPtr &msg){
    gps_en = msg->data;
//    ROS_INFO("start trigger sub");
}
*/

bool trigger(muin_px4::start_trig::Request &req,muin_px4::start_trig::Response &res){
    gps_en=req.trigger;
    res.success=gps_en;
    ROS_INFO("service check : %d!",gps_en);
    return true;
}

void cb_m_GPS_raw(const sensor_msgs::NavSatFix::ConstPtr &msg)
{
    GPS_data = *msg;
    if(gps_en^gps_en_past){
        ROS_INFO("start");
        start_point.geo.altitude=GPS_data.altitude;
        start_point.geo.latitude=GPS_data.latitude;
        start_point.geo.longitude=GPS_data.longitude;
        local_x_start=local_data.pose.position.x;
        local_y_start=local_data.pose.position.y;
        local_z_start=local_data.pose.position.z;
	gps_en_past=gps_en;
    }
//    ROS_ERROR("altitude : %lf", start_point.geo.altitude);
//    ROS_ERROR("latitude : %lf", start_point.geo.latitude);
//    ROS_ERROR("longitude : %lf", start_point.geo.longitude);
//    ROS_ERROR("altitude : %lf", GPS_data.altitude);
//    ROS_ERROR("latitude : %lf", GPS_data.latitude);
//    ROS_ERROR("longitude : %lf", GPS_data.longitude);
//    ROS_INFO("GPS sub");
}

void cb_m_local(const geometry_msgs::PoseStamped::ConstPtr &msg)
{
    local_data = *msg;
//    ROS_INFO("local sub");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "local2gps");
    ros::NodeHandle nh;
    ros::Rate pub_rate(10);
    ////    service topic subscriber
//    ros::Subscriber sub_m_start_trigger = nh.subscribe<std_msgs::Bool>("/start_trigger",1,cb_m_start_trigger);
    ros::Subscriber sub_m_poseGlobal = nh.subscribe<sensor_msgs::NavSatFix>("/mavros/global_position/raw/fix",1, /*&muin_class::*/cb_m_GPS_raw);
    ros::Subscriber sub_m_poseLocal = nh.subscribe<geometry_msgs::PoseStamped>("/mavros/local_position/pose", 1, /*&muin_class::*/cb_m_local);
    ////    service topic publisher
    ros::Publisher image_location = nh.advertise<sensor_msgs::NavSatFix>("/estimated_global_position",1000);
    ros::Publisher debug_start_point = nh.advertise<mavros_msgs::HomePosition>("/debug/start_point",1000);
    ////    service service publisher
    ros::ServiceServer start_trigger = nh.advertiseService("start_trigger",trigger);
    while(ros::ok()){
        ros::spinOnce();
      //  ROS_INFO("%d",gps_en^gps_en_past);
        if(gps_en){
            debug_start_point.publish(start_point);
            estimated_data.altitude = start_point.geo.altitude;
            estimated_data.header.frame_id=local_data.header.frame_id;
            estimated_data.header.seq=estimated_data_seq;
            estimated_data.header.stamp=local_data.header.stamp;
            double local_x=local_data.pose.position.x-local_x_start;
            double local_y=local_data.pose.position.y-local_y_start;
            double local_z=local_data.pose.position.z-local_z_start;

            double utm_x;
            double utm_y;

            int zone = UTM::UTM_ZONE(start_point.geo.longitude);
            LatLonToUTMXY(start_point.geo.latitude, start_point.geo.longitude, zone,utm_x, utm_y);
            utm_x+=local_x;
            utm_y+=local_y;
            UTMXYToLatLon(utm_x,utm_y,zone,false,estimated_data.latitude,estimated_data.longitude);
            estimated_data.altitude += local_z;
            estimated_data_seq ++;
            image_location.publish(estimated_data);
//            ROS_INFO("estimated latitude : %lf", estimated_data.latitude);
//            ROS_INFO("estimated longitude : %lf", estimated_data.longitude);
//            ROS_INFO("estimated altitude : %lf", estimated_data.altitude);
        }
        else{
            if(!gps_en) {
              //  ROS_INFO("Not started, publish topic /start_trigger");
            }
        }
        pub_rate.sleep();
    }
}
