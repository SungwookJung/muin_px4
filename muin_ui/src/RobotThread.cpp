#include "RobotThread.h"

RobotThread::RobotThread(int argc, char** pArgv, const char * topic)
    :	m_Init_argc(argc),
        m_pInit_argv(pArgv),
        m_topic(topic)
{/** Constructor for the robot thread **/}

RobotThread::~RobotThread()
{
    if (ros::isStarted())
    {
        ros::shutdown();
        ros::waitForShutdown();
    }//end if

    m_pThread->wait();
}//end destructor

bool RobotThread::init()
{
    m_pThread = new QThread();
    this->moveToThread(m_pThread);

    connect(m_pThread, &QThread::started, this, &RobotThread::run);
    ros::init(m_Init_argc, m_pInit_argv, "gui_command");

    if (!ros::master::check())
        return false;//do not start without ros.

    ros::start();
    ros::Time::init();
    ros::NodeHandle nh;

//  subscribe //
    sim_velocity  = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 100);
    sub_pix_diagnostic = nh.subscribe("/diagnostics",1, &RobotThread::gpssatCallback, this);
    sub_estgps = nh.subscribe("/estimated_global_position",1,&RobotThread::estgpsCallback, this);
    sub_localpose = nh.subscribe("/mavros/local_position/pose", 1, &RobotThread::localposeCallback, this);
    sub_imu = nh.subscribe("/mavros/imu/data",1, &RobotThread::imuCallback, this);
    sub_status = nh.subscribe("/mavros/statustext/recv", 1, &RobotThread::statusCallback, this);
    sub_battery = nh.subscribe("/mavros/battery", 1, &RobotThread::batteryCallback, this);
    sub_state = nh.subscribe("/mavros/state", 1, &RobotThread::mavstateCallback, this);
    sub_logdata = nh.subscribe("/muin/log_data", 1, &RobotThread::logdataCallback, this);

// service //
    srv_take_off            = nh.serviceClient<muin_px4::take_off>("take_off");
    srv_landing             = nh.serviceClient<muin_px4::landing>("landing");
    srv_emergency_landing   = nh.serviceClient<muin_px4::emergency_landing>("emergency_landing");
    srv_pause_mission       = nh.serviceClient<muin_px4::pause_mission>("pause_mission");
    srv_record_start        = nh.serviceClient<muin_px4::test_srv>("test_srv");
    srv_record_stop         = nh.serviceClient<muin_px4::test_srv>("test_srv");
    srv_local2gps           = nh.serviceClient<muin_px4::start_trig>("start_trigger");
    srv_next_mission        = nh.serviceClient<muin_px4::next_mission>("next_mission");
    srv_prev_mission        = nh.serviceClient<muin_px4::previous_mission>("previous_mission");
    srv_upload_mission      = nh.serviceClient<muin_px4::ui_mission_request>("send_mission_info");
    srv_nonstop_mission     = nh.serviceClient<muin_px4::automatic_mission_start>("automatic_mission_start");
    m_pThread->start();
    return true;
}//set up the thread


void RobotThread::run()
{
    ros::Rate loop_rate(100);
    QMutex * pMutex;
    while (ros::ok())
    {
        pMutex = new QMutex();

        geometry_msgs::Twist cmd_msg;
        pMutex->lock();
        cmd_msg.linear.x = m_speed;
        cmd_msg.angular.z = m_angle;
        pMutex->unlock();

        sim_velocity.publish(cmd_msg);
        ros::spinOnce();
        loop_rate.sleep();
        delete pMutex;
    }//do ros things.
}


double RobotThread::getXSpeed(){ return m_speed; }
double RobotThread::getASpeed(){ return m_angle; }
double RobotThread::getXPos(){ return m_xPos; }
double RobotThread::getYPos(){ return m_yPos; }
double RobotThread::getAPos(){ return m_zPos; }



void RobotThread::gpssatCallback(const diagnostic_msgs::DiagnosticArray::ConstPtr &msg)
{
  QMutex * pMutex = new QMutex();
  pMutex->lock();
  pix_diagnostic = *msg;
  std::string tmp_s;
  for(int i = 0; i < pix_diagnostic.status.size(); i++)
  {
    tmp_s = pix_diagnostic.status[i].name;
    if(tmp_s == "mavros: GPS") pix_diagnostic_GPS = pix_diagnostic.status[i];
  }

  for(int idx = 0; idx < pix_diagnostic_GPS.values.size(); idx++)
  {
    if(pix_diagnostic_GPS.values[idx].key == "Satellites visible")
    {
      std::string tmp_s = pix_diagnostic_GPS.values[idx].value;
      satecount = std::atoi(tmp_s.c_str());
    }
  }
  pMutex->unlock();
  delete pMutex;
  Q_EMIT gpscount(satecount);
}

void RobotThread::statusCallback(const mavros_msgs::StatusText::ConstPtr &msg)
{
  QMutex * pMutex = new QMutex();
  pMutex->lock();
  std::string tmp_s;
  tmp_s = msg->text;
  ROS_INFO("%s", tmp_s.c_str());
  pMutex->unlock();
  delete pMutex;
  Q_EMIT status(tmp_s);
}

void RobotThread::logdataCallback(const muin_px4::log_data::ConstPtr &msg)
{
  QMutex * pMutex = new QMutex();
  pMutex->lock();
  std::string tmp_s;
  tmp_s = msg->log_string;
  ROS_INFO("%s", tmp_s.c_str());
  pMutex->unlock();
  delete pMutex;
  Q_EMIT logdata(tmp_s);
}

void RobotThread::mavstateCallback(const mavros_msgs::State::ConstPtr &msg)
{
  QMutex * pMutex = new QMutex();
  pMutex->lock();
  std::string mode;
  bool armed;
  std::string armStr;

  if (msg->armed == 1) (armStr="OK");
  else (armStr="NOT");
  mode = msg->mode;
  pMutex->unlock();
  delete pMutex;
  Q_EMIT state(armStr, mode);
}


void RobotThread::estgpsCallback(const sensor_msgs::NavSatFix &msg)
{
  QMutex * pMutex = new QMutex();
  pMutex->lock();
  lon = msg.longitude;
  lat = msg.latitude;
  alt = msg.altitude;
  pMutex->unlock();
  delete pMutex;
  Q_EMIT estGPS(lon, lat, alt);
}

void RobotThread::imuCallback(const sensor_msgs::Imu &msg)
{
  QMutex * pMutex = new QMutex();
  pMutex->lock();
  double orientation[4] = {0};
  orientation[0] = msg.orientation.x;
  orientation[1] = msg.orientation.y;
  orientation[2] = msg.orientation.z;
  orientation[3] = msg.orientation.w;
  tf::Quaternion q(orientation[0], orientation[1], orientation[2], orientation[3]);
  tf::Matrix3x3 m(q);
  m.getRPY(rR, rP, rY);
  m_heading = rY*180./M_PI;
  pMutex->unlock();
  delete pMutex;
  Q_EMIT imu(m_heading);

}

void RobotThread::localposeCallback(const geometry_msgs::PoseStamped::ConstPtr &msg)
{
  QMutex * pMutex = new QMutex();
  pMutex->lock();
  m_xPos = msg->pose.position.x;
  m_yPos = msg->pose.position.y;
  m_zPos = msg->pose.position.z;
  geometry_msgs::Quaternion q = msg->pose.orientation;
  geometry_msgs::Vector3 res;
  tf::Matrix3x3 R_FLU2ENU(tf::Quaternion(q.x, q.y, q.z, q.w));
  R_FLU2ENU.getRPY(res.x, res.y, res.z);
  m_tPos = res.z*180 / M_PI;
  pMutex->unlock();
  delete pMutex;
  Q_EMIT localpose(m_xPos, m_yPos, m_zPos, m_tPos);
}

void RobotThread::batteryCallback(const sensor_msgs::BatteryState::ConstPtr &msg)
{
  QMutex * pMutex = new QMutex();
  pMutex->lock();
  m_batt = msg->percentage*100;
  pMutex->unlock();
  delete pMutex;
  Q_EMIT battery(m_batt);
}


/*
 * Service Functions
 *
 */

void RobotThread::fn_take_off()
{
  muin_px4::take_off srv;
  srv_take_off.call(srv);
  ROS_INFO("%d",srv.response.result);
  ROS_INFO("takeoff");
}

void RobotThread::fn_landing()
{
  muin_px4::landing srv;
  srv_landing.call(srv);
  ROS_INFO("%d",srv.response.result);
  ROS_INFO("landing");
}

void RobotThread::fn_emergency_landing()
{
  muin_px4::emergency_landing srv;
  srv_emergency_landing.call(srv);
  ROS_INFO("%d",srv.response.result);
  ROS_INFO("e-landing");
}

void RobotThread::fn_pause_mission()
{
  muin_px4::pause_mission srv;
  srv_pause_mission.call(srv);
  ROS_INFO("%d",srv.response.result);
  ROS_INFO("pause");
}

void RobotThread::fn_next_mission()
{
  muin_px4::next_mission srv;
  srv_next_mission.call(srv);
  ROS_INFO("%d",srv.response.result);
  ROS_INFO("next");
}

void RobotThread::fn_prev_mission()
{
  muin_px4::previous_mission srv;
  srv_prev_mission.call(srv);
  ROS_INFO("%d",srv.response.result);
  ROS_INFO("previous");
}

void RobotThread::fn_record_start()
{
  muin_px4::record_start srv;
  srv.request.value=1;
  srv_record_start.call(srv);
  ROS_INFO("%d",srv.response.result);
  ROS_INFO("cam_start");
}

void RobotThread::fn_record_stop()
{
  muin_px4::record_stop srv;
  srv.request.value=2;
  srv_record_stop.call(srv);
  ROS_INFO("%d",srv.response.result);
  ROS_INFO("cam_stop");
}

void RobotThread::fn_upload_mission()
{
  muin_px4::ui_mission_request srv;
  srv_upload_mission.call(srv);
  ROS_INFO("%d",srv.response.complete);
  ROS_INFO("mission upload");
}

void RobotThread::fn_local2gps()
{
  muin_px4::start_trig srv;
  srv.request.trigger = true;
  srv_local2gps.call(srv);
  ROS_INFO("%d",srv.response.success);
  ROS_INFO("local2gps");
}

void RobotThread::fn_nonstop_mission()
{
  muin_px4::automatic_mission_start srv;
  srv_nonstop_mission.call(srv);
  ROS_INFO("%d",srv.response.result);
  ROS_INFO("nonstop mission");
}


