#ifndef CONTROL_WINDOW_H
#define CONTROL_WINDOW_H
#include <QtWidgets>
#include <QPushButton>
#include <QString>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QTextStream>
#include <QLineEdit>
#include <QPalette>
#include <QIcon>
#include "RobotThread.h"
#include <iostream>
#include <QListView>

namespace server{
#define PI 3.14159265359
//Q_DECLARE_METATYPE (std::string)

class ControlWindow : public QWidget
{
    Q_OBJECT

public:
    ControlWindow(int argc, char** argv, QWidget * parent = 0);

    Q_SLOT void updatePoseDisplay(double x, double y, double z, double theta);
    Q_SLOT void satelliteDisplay(unsigned int count);
    Q_SLOT void estGPSDisplay(double lon, double lat, double alt);
    Q_SLOT void imuDisplay(double heading);
    Q_SLOT void logDisplay(std::string log);
    Q_SLOT void batDisplay(double bat);
    Q_SLOT void stateDisplay(std::string arm, std::string mode);

  //  qRegisterMetaType<std::std>


private:
    Q_SLOT void muin_takeoff();
    Q_SLOT void muin_landing();
    Q_SLOT void muin_elanding();
    Q_SLOT void muin_missionpause();
    Q_SLOT void muin_camstart();
    Q_SLOT void muin_camstop();
    Q_SLOT void muin_local2gps();
    Q_SLOT void muin_nextmission();
    Q_SLOT void muin_prevmission();
    Q_SLOT void muin_missionupload();
    Q_SLOT void muin_nonstopmission();

   // Q_SLOT void halt();

    QPushButton *p_takeoffButton;
    QPushButton *p_landingButton;
    QPushButton *p_elandingButton;
    QPushButton *p_missionstartButton;
    QPushButton *p_missionpauseButton;
    QPushButton *p_missionstopButton;
    QPushButton *p_missionuploadButton;
    QPushButton *p_sethome;
    QPushButton *p_local2gps;
    QPushButton *p_nextwaypoint;
    QPushButton *p_previouswaypoint;
    QPushButton *p_camstartButton;
    QPushButton *p_camstopButton;
    QPushButton *p_quitButton;

    QPushButton *p_throttleup;
    QPushButton *p_throttledown;
    QPushButton *p_cwyaw;
    QPushButton *p_ccwyaw;
    QPushButton *p_forwardButton;
    QPushButton *p_backwardButton;
    QPushButton *p_leftButton;
    QPushButton *p_rightButton;
    QPushButton *p_logoButton;

    QTextBrowser *ploglog;

  //  std::string log;



    QVBoxLayout *rightLayout;
    QHBoxLayout *layout;
    QHBoxLayout *layout2;
    QHBoxLayout *layout3;
    QHBoxLayout *layout4;
    QHBoxLayout *layout5;
    QHBoxLayout *layout6;
    QHBoxLayout *layout7;
    QHBoxLayout *layout10;
    QHBoxLayout *layout8;
    QHBoxLayout *layout9;

    QVBoxLayout *leftLayout;

    // local pose
    QHBoxLayout *p_xLayout;
    QHBoxLayout *p_yLayout;
    QHBoxLayout *p_zLayout;
    QHBoxLayout *p_thetaLayout;
    // est. GPS
    QHBoxLayout *p_estlatLayout;
    QHBoxLayout *p_estlonLayout;
    QHBoxLayout *p_estaltLayout;
    QHBoxLayout *p_imuLayout;

    QHBoxLayout *p_satelliteLayout;
    QHBoxLayout *p_batteryLayout;
    QHBoxLayout *p_flighttimeLayout;
    QHBoxLayout *p_armLayout;
    QHBoxLayout *p_modeLayout;
    QHBoxLayout *p_logLayout;
    QHBoxLayout *p_llogLayout;

    QHBoxLayout *p_logoLayout;

    QLabel *p_xLabel;
    QLabel *p_yLabel;
    QLabel *p_zLabel;
    QLabel *p_thetaLabel;

    QLabel *p_estlatLabel;
    QLabel *p_estlonLabel;
    QLabel *p_estaltLabel;
    QLabel *p_imuLabel;
    QLabel *p_satelliteLabel;
    QLabel *p_batteryLabel;
    QLabel *p_flighttimeLabel;
    QLabel *p_armLable;
    QLabel *p_modeLable;

    QLabel *p_loglogLabel;



    QLabel *p_scanLabel;

    QLineEdit *p_xDisplay;
    QLineEdit *p_yDisplay;
    QLineEdit *p_zDisplay;
    QLineEdit *p_thetaDisplay;


    QLineEdit *p_estlatDisplay;
    QLineEdit *p_estlonDisplay;
    QLineEdit *p_estaltDisplay;

    QLineEdit *p_imuDisplay;
    QLineEdit *p_armDisplay;
    QLineEdit *p_modeDisplay;

    QLineEdit *p_satelliteDisplay;
    QLineEdit *p_batteryDisplay;
    QLineEdit *p_flighttimeDisplay;


    QVBoxLayout *bottomLayout;
    QPixmap logo;



    QHBoxLayout *mainLayout;
    QPushButton *closeButton;

    RobotThread m_RobotThread;
};//end class ControlWindow
}//end namespace
#endif

